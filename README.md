# Android testcase (Junior)

## Task
Finish music application.

## Description
Application have a music artist list and detailed info about each artist.
You will find 15 TODO in code.

First screen - list of popular music artists.

Second screen - detailed info about selected music artist.

Initial data on the network application receives the JSON from fake API. No tests needed

## Aprox. time
1-2 hours

## Result
Result on github.com/gitlab.com (open repository) and a link to the email.
package com.dhl.fsi.testcase.views;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dhl.fsi.testcase.R;


public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Context context;
    private Paint   paint;

    public SimpleDividerItemDecoration(Context context) {
        this.context = context;
        init();

    }

    // initialize paint object
    private void init() {
        paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(context.getResources().getColor(R.color.divider_color));
        paint.setStrokeWidth(context.getResources().getDimension(R.dimen.divider_width));
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left       = parent.getPaddingLeft();
        int right      = parent.getWidth() - parent.getPaddingRight();
        int childCount = parent.getChildCount();
        // for every row in recyclerView
        for (int i = 0; i < childCount; i++) {
            View                      child  = parent.getChildAt(i);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int                       top    = child.getBottom() + params.bottomMargin;
            // draw line from left to right in the top of child view
            c.drawLine(left, top, right, top, paint);
        }
    }
}


package com.dhl.fsi.testcase.fragments;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.model.Artist;
import com.dhl.fsi.testcase.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsArtistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsArtistFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String ARTIST_ID             = "artist_id";
    private static final int    ARTIST_DETAILS_LOADER = 2;
    private Artist            artist;
    private long              artistId;
    private CoordinatorLayout coordinatorLayout;
    private ImageView         coverImageView;
    private ProgressBar       coverProgressBar;
    private TextView          descriptionTextView;
    private TextView          genresTextView;
    private TextView          linkTextView;
    private Toolbar           toolbar;
    private TextView          tracksAlbumsTextView;


    public DetailsArtistFragment() {
        // Required empty public constructor
    }


    public static DetailsArtistFragment newInstance(long id) {
        DetailsArtistFragment fragment = new DetailsArtistFragment();
        Bundle                args     = new Bundle();
        args.putLong(ARTIST_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    // add or remove artist to/from favorites
    private void addOrRemoveFavorites(MenuItem item) {
        if (item.isChecked()) {
            // delete row where artist_id = selected id from favorite table
            getActivity().getContentResolver().delete(ArtistContract.FavoriteEntry.CONTENT_URI,
                                                      ArtistContract.FavoriteEntry.COLUMN_ID + " = ?",
                                                      new String[]{String.valueOf(artistId)});
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ArtistContract.FavoriteEntry.COLUMN_ID, artistId);
            // add id to favorite table
            getActivity().getContentResolver().insert(ArtistContract.FavoriteEntry.CONTENT_URI,
                                                      contentValues);
        }

        // change icon
        item.setIcon(item.isChecked() ? R.drawable.ic_star_outline_white_24dp : R.drawable.ic_star_white_24dp);
        item.setChecked(!item.isChecked());
    }

    /*
     return true if artistId are in favorites table
      */
    private boolean isArtistInFavorites() {
        Cursor c = getActivity().getContentResolver().query(ArtistContract.FavoriteEntry.CONTENT_URI,
                                                            null,
                                                            ArtistContract.FavoriteEntry.COLUMN_ID + " = ?",
                                                            new String[]{String.valueOf(artistId)},
                                                            null);
        boolean isFavorite = (c != null && c.getCount() > 0);
        if (c != null) {
            c.close();
        }
        return isFavorite;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            artistId = getArguments().getLong(ARTIST_ID);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details_artist, container, false);
        // set up toolbar
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        //TODO: Replace next with Butterknife

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);
        coverImageView = (ImageView) view.findViewById(R.id.cover_image_view);
        genresTextView = (TextView) view.findViewById(R.id.genres_text_view);
        tracksAlbumsTextView = (TextView) view.findViewById(R.id.album_text_view);
        descriptionTextView = (TextView) view.findViewById(R.id.description_text_view);
        linkTextView = (TextView) view.findViewById(R.id.link_text_view);
        coverProgressBar = (ProgressBar) view.findViewById(R.id.cover_progress_bar);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(ARTIST_DETAILS_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_details_fragment, menu);
        MenuItem item = menu.findItem(R.id.action_favorites);
        item.setChecked(isArtistInFavorites());
        item.setIcon(item.isChecked() ? R.drawable.ic_star_white_24dp : R.drawable.ic_star_outline_white_24dp);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorites:
                addOrRemoveFavorites(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // update data in views
    private void updateData() {
        coverProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(getActivity())
               .load(artist.getCover().getBig())
               .error(R.drawable.alert_circle)
               .into(coverImageView, new Callback() {
                   @Override
                   public void onSuccess() {
                       //TODO: implement success image loading
                       // animate showing cover image
                       Utils.animate(coverImageView, Utils.Gravity.RIGHT, Utils.Gravity.TOP, 500);
                   }

                   @Override
                   public void onError() {
                       //TODO: implement fail image loading
                       // if error on loading,
                       // check internet connection & show msg
                       Utils.showSnackBarIfInternetIsMissing(getActivity(), coordinatorLayout);

                   }
               });

        //formating string in correct rules
        String trackAlbumCount = getResources().getQuantityString(R.plurals.plural_album, artist.getAlbums(), artist.getAlbums())
                                 + "  \u2022  " + getResources().getQuantityString(R.plurals.plural_song, artist.getTracks(), artist.getTracks());

        //TODO: attach text to view
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // load one artist by id
        return new CursorLoader(getActivity(),
                                ArtistContract.ArtistEntry.buildArtistUri(artistId),
                                null,
                                null,
                                null,
                                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            artist = ArtistContract.convertCursorToArtist(cursor);
            updateData();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}

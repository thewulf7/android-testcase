package com.dhl.fsi.testcase.retrofit;

import com.dhl.fsi.testcase.model.Artist;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

public interface API {
    @GET("master/artists.json")
    Observable<Response<List<Artist>>> getArtists();
}

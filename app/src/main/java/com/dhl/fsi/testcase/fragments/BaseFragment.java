package com.dhl.fsi.testcase.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.adapters.ArtistAdapter;
import com.dhl.fsi.testcase.adapters.ArtistAdapter.OnClickArtistListener;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.views.SimpleDividerItemDecoration;
import com.lapism.searchview.SearchHistoryTable;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;

public class BaseFragment extends Fragment {

    private static final int ARTIST_SUGGESTION_LOADER = 1;

    ArtistAdapter      adapter;
    RecyclerView       recyclerView;
    List<SearchItem>   suggestionsList;
    SearchHistoryTable historyDatabase;
    SearchView         searchView;
    Toolbar            toolbar;

    private OnClickArtistListener onClickArtistListener = new OnClickArtistListener() {
        @Override
        public void onClickArtist(long artistId) {
            //TODO: open detail artist screen
        }
    };

    void showSearchView() {
        suggestionsList.clear();
        suggestionsList.addAll(historyDatabase.getAllItems(null));

        // load name for suggestion
        getLoaderManager().initLoader(ARTIST_SUGGESTION_LOADER, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                return new CursorLoader(getActivity(),
                                        ArtistContract.ArtistEntry.CONTENT_URI,
                                        new String[]{ArtistContract.ArtistEntry.COLUMN_NAME},
                                        null,
                                        null,
                                        null);
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                if (data.moveToFirst()) {
                    do {
                        suggestionsList.add(new SearchItem(data.getString(
                                data.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_NAME))));
                    } while (data.moveToNext());
                }

            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {

            }
        });

        searchView.open(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                showSearchView();
                return true;
            case R.id.action_favorites:
                //TODO: open favorites screen
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void initRecyclerView(View view) {
        // TODO: set up RecyclerView
    }


    // set up SearchView
    void initSearchView(View view) {
        searchView = (SearchView) view.findViewById(R.id.searchView);
        historyDatabase = new SearchHistoryTable(getActivity());
        suggestionsList = new ArrayList<>();

        int mVersion = SearchView.VERSION_MENU_ITEM;
        int mTheme   = SearchView.THEME_LIGHT;

        // required settings
        searchView.setVersion(mVersion);
        searchView.setTheme(mTheme);

        // not required settings
        searchView.setDivider(false);
        searchView.setHint(R.string.search);
        searchView.setVoice(false);
        searchView.setAnimationDuration(300);
        searchView.setShadowColor(getResources().getColor(R.color.search_shadow_layout));
    }

    void initToolbar(View view, int titleResId) {
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setTitle(getResources().getString(titleResId));
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }


}

package com.dhl.fsi.testcase.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Evgenii Utkin
 */
class ArtistDbHelper extends SQLiteOpenHelper {
    private static final int    DATABASE_VERSION = 1;
    private static final String DATABASE_NAME    = "artist.db";

    public ArtistDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
    * Create two tables: artists and favorites
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_ARTIST_TABLE = "CREATE TABLE " + ArtistContract.ArtistEntry.TABLE_NAME
                                               + " (" + ArtistContract.ArtistEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                               + ArtistContract.ArtistEntry.COLUMN_ID + " INTEGER, "
                                               + ArtistContract.ArtistEntry.COLUMN_NAME + " TEXT NOT NULL, "
                                               + ArtistContract.ArtistEntry.COLUMN_GENRES + " TEXT, "
                                               + ArtistContract.ArtistEntry.COLUMN_TRACKS + " INTEGER, "
                                               + ArtistContract.ArtistEntry.COLUMN_ALBUMS + " INTEGER, "
                                               + ArtistContract.ArtistEntry.COLUMN_LINK + " TEXT, "
                                               + ArtistContract.ArtistEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, "
                                               + ArtistContract.ArtistEntry.COLUMN_COVER_SMALL + " TEXT NOT NULL, "
                                               + ArtistContract.ArtistEntry.COLUMN_COVER_BIG + " TEXT NOT NULL)";

        db.execSQL(SQL_CREATE_ARTIST_TABLE);

        final String SQL_CREATE_FAVORITES_TABLE = "CREATE TABLE " + ArtistContract.FavoriteEntry.TABLE_NAME
                                                  + " (" + ArtistContract.FavoriteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                                  + ArtistContract.FavoriteEntry.COLUMN_ID + " INTEGER)";

        db.execSQL(SQL_CREATE_FAVORITES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ArtistContract.ArtistEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ArtistContract.FavoriteEntry.TABLE_NAME);
        onCreate(db);
    }
}

package com.dhl.fsi.testcase.utils;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.dhl.fsi.testcase.R;

public class Utils {

    /**
     * @return true if internet is connected
     */
    public static boolean checkInternetConnection(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null
               && connectivityManager.getActiveNetworkInfo().isAvailable()
               && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /*
    show SnackBar if internet isn't connected
     */
    public static void showSnackBarIfInternetIsMissing(Activity activity, CoordinatorLayout coordinatorLayout) {
        if (!Utils.checkInternetConnection(activity)) {
            Snackbar.make(coordinatorLayout, activity.getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Make circular reveal animation
     *
     * @param view     - view to animate.
     * @param x        - gravity RIGHT or LEFT where the center of the Circular Reveal
     * @param y        - y coordinate of the center of the Circular Reveal
     * @param duration - duration of the animation
     */
    public static void animate(View view, final Gravity x, final Gravity y, final int duration) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);

                    int cx = 0;
                    int cy = 0;
                    switch (x) {
                        case RIGHT:
                            cx = v.getRight();
                            break;
                        case LEFT:
                            cx = v.getLeft();
                            break;
                        case CENTER_HORIZONTAL:
                            cx = (v.getLeft() + v.getRight()) / 2;
                            break;
                    }
                    switch (y) {
                        case TOP:
                            cy = v.getTop();
                            break;
                        case BOTTOM:
                            cy = v.getBottom();
                            break;
                        case CENTER_VERTICAL:
                            cy = (v.getTop() + v.getBottom()) / 2;
                    }

                    // get the final radius for the clipping circle
                    int finalRadius = Math.max(v.getWidth() * 2, v.getHeight() * 2);

                    // create the animator for this view (the start radius is zero)
                    Animator anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0, finalRadius);

                    // make the view visible and start the animation
                    v.setVisibility(View.VISIBLE);
                    anim.setDuration(duration);
                    anim.start();
                }
            });

        }
    }

    public enum Gravity {
        TOP, BOTTOM, LEFT, RIGHT, CENTER_HORIZONTAL, CENTER_VERTICAL
    }


}

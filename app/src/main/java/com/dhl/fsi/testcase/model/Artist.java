package com.dhl.fsi.testcase.model;

import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

public class Artist {

    @SerializedName("albums")
    private int          albums;
    @SerializedName("cover")
    private Cover        cover;
    @SerializedName("description")
    private String       description;
    @SerializedName("genres")
    private List<String> genres;
    @SerializedName("id")
    private long         id;
    @SerializedName("link")
    private String       link;
    @SerializedName("name")
    private String       name;
    @SerializedName("tracks")
    private int          tracks;


    public Artist(long id, String name, List<String> genres, int tracks, int albums, String link, String description, Cover cover) {
        this.id = id;
        this.name = name;
        this.genres = genres;
        this.tracks = tracks;
        this.albums = albums;
        this.link = link;
        this.description = description;
        this.cover = cover;
    }

    public String getGenresString() {
        StringBuilder    stringBuilder = new StringBuilder();
        Iterator<String> iterator      = genres.iterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            if (iterator.hasNext()) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    public List<String> getGenres() {
        return genres;
    }


    public int getTracks() {
        return tracks;
    }


    public int getAlbums() {
        return albums;
    }


    public String getLink() {
        return link;
    }


    public String getDescription() {
        return description;
    }


    public Cover getCover() {
        return cover;
    }
}

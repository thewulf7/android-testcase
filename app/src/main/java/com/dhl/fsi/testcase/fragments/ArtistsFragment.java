package com.dhl.fsi.testcase.fragments;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhl.fsi.testcase.BuildConfig;
import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.model.Artist;
import com.dhl.fsi.testcase.retrofit.API;
import com.dhl.fsi.testcase.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int ARTIST_LOADER = 0;

    private API                api;
    private CoordinatorLayout  coordinatorLayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ArtistsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Gson gson = new GsonBuilder().setLenient().create();

        //setup okhttp client
        OkHttpClient client = new OkHttpClient.Builder()
                .sslSocketFactory(getSSLSocketFactory())
                .build();

        // set up Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        api = retrofit.create(API.class);
    }

    // set up SearchView
    @Override
    void initSearchView(View view) {
        super.initSearchView(view);

        List<SearchItem> mResultsList = new ArrayList<>();
        // set text change listener
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.close(false);
                historyDatabase.addItem(new SearchItem(query));
                // TODO: start SearchResultFragment
                return false;
            }
        });

        SearchAdapter mSearchAdapter = new SearchAdapter(getActivity(), mResultsList);
        // listener for click on suggestion
        mSearchAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                searchView.close(false);
                // get query from list element which we've selected
                TextView     textView = (TextView) view.findViewById(R.id.textView_item_text);
                CharSequence text     = textView.getText();
                // adding to history
                try {
                    historyDatabase.addItem(new SearchItem(text));
                } catch (CursorIndexOutOfBoundsException e) {
                    Log.d("DB", e.toString());
                }
                // TODO: start SearchResultFragment
            }
        });

        searchView.setAdapter(mSearchAdapter);
    }

    /**
     * Special trick for internal DHL gitlab
     *
     * @return
     */
    private SSLSocketFactory getSSLSocketFactory() {

        SSLContext sslContext = null;

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            Log.d("SSL", "Self-signed SSL certificate problem - " + e.getMessage());
        }

        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_artists, container, false);

        // set up toolbar
        initToolbar(view, R.string.artists);

        // set up SearchView
        initSearchView(view);
        // Set up RecyclerView
        initRecyclerView(view);

        // TODO: set up SwipeRefreshLayout

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);

        loadArtists();

        // animate showing fragment
        Utils.animate(view, Utils.Gravity.LEFT, Utils.Gravity.TOP, 500);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(ARTIST_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    /*
        load data from internet
        and write to database
     */
    private void loadArtists() {

        api.getArtists()
           .subscribeOn(Schedulers.io())
           .unsubscribeOn(Schedulers.io())
           .observeOn(AndroidSchedulers.mainThread())
           .subscribe(new Subscriber<Response<List<Artist>>>() {
               @Override
               public void onCompleted() {
                   Log.d("TEST", "Request complete!");
               }

               @Override
               public void onError(Throwable e) {
                   if (!Utils.checkInternetConnection(getActivity())) {
                       Snackbar.make(coordinatorLayout, getResources().getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show();
                   }
                   //TODO: notify SwipeRefreshLayout
               }

               @Override
               public void onNext(Response<List<Artist>> response) {
                   final List<Artist> artists = response.body();

                   //TODO: notify SwipeRefreshLayout

                   // write artists to database
                   final ContentResolver contentResolver = getActivity().getContentResolver();

                   new AsyncTask<Void, Void, Void>() {
                       @Override
                       protected Void doInBackground(Void... params) {
                           ContentValues[] values = new ContentValues[artists.size()];
                           for (int i = 0; i < artists.size(); i++) {
                               values[i] = ArtistContract.convertArtistToContentValues(artists.get(i));
                           }

                           //TODO: remove&insert data to content resolver
                           return null;
                       }

                       @Override
                       protected void onPostExecute(Void aVoid) {
                           contentResolver.notifyChange(ArtistContract.ArtistEntry.CONTENT_URI, null);
                       }
                   }.execute();
               }
           });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // load all artists
        return new CursorLoader(getActivity(),
                                ArtistContract.ArtistEntry.CONTENT_URI,
                                null,
                                null,
                                null,
                                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}

package com.dhl.fsi.testcase.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.model.Artist;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistViewHolder> {
    private Context               context;
    private Cursor                cursor;
    private OnClickArtistListener onClickArtistListener;


    public ArtistAdapter(Context context, OnClickArtistListener onClickArtistListener) {
        this.context = context;
        this.onClickArtistListener = onClickArtistListener;
    }

    @Override
    public ArtistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.artist_item, parent, false);
        return new ArtistViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ArtistViewHolder holder, int position) {

        final Cursor cursor = this.getItem(position);
        // create artist object from cursor
        final Artist artist = ArtistContract.convertCursorToArtist(cursor);
        holder.nameTextView.setText(artist.getName());

        holder.genresTextView.setText(artist.getGenresString());

        //formating string according to quantity
        String trackAlbumCount = context.getResources().getQuantityString(R.plurals.plural_album, artist.getAlbums(), artist.getAlbums())
                                 + ", " + context.getResources().getQuantityString(R.plurals.plural_song, artist.getTracks(), artist.getTracks());

        holder.tracksAlbumsTextView.setText(trackAlbumCount);
        holder.coverProgressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
               .load(artist.getCover().getSmall())
               .error(R.drawable.alert_circle)
               .into(holder.coverImageView, new Callback() {
                   @Override
                   public void onSuccess() {
                       holder.coverProgressBar.setVisibility(View.GONE);
                   }

                   @Override
                   public void onError() {
                       holder.coverProgressBar.setVisibility(View.GONE);
                   }
               });

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickArtistListener.onClickArtist(artist.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.cursor != null
               ? this.cursor.getCount()
               : 0;
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        this.notifyDataSetChanged();
    }

    private Cursor getItem(int position) {
        if (this.cursor != null && !this.cursor.isClosed()) {
            this.cursor.moveToPosition(position);
        }
        return this.cursor;
    }

    public interface OnClickArtistListener {
        void onClickArtist(long artistId);
    }

    public static class ArtistViewHolder extends RecyclerView.ViewHolder {
        TextView    nameTextView;
        TextView    genresTextView;
        TextView    tracksAlbumsTextView;
        ProgressBar coverProgressBar;
        ImageView   coverImageView;
        View        mainView;


        ArtistViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            genresTextView = (TextView) itemView.findViewById(R.id.genres_text_view);
            tracksAlbumsTextView = (TextView) itemView.findViewById(R.id.tracks_albums_text_view);
            coverProgressBar = (ProgressBar) itemView.findViewById(R.id.cover_progress_bar);
            coverImageView = (ImageView) itemView.findViewById(R.id.cover_image_view);
            mainView = itemView;
        }
    }
}

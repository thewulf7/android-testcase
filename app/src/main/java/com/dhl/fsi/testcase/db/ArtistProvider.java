package com.dhl.fsi.testcase.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Evgenii Utkin
 */
@SuppressWarnings("ConstantConditions")
public class ArtistProvider extends ContentProvider {
    private static final int        ARTIST       = 100;
    private static final int        ARTIST_BY_ID = 101;
    private static final int        FAVORITES    = 102;
    private static final UriMatcher sUriMatcher  = buildUriMatcher();
    private ArtistDbHelper artistDbHelper;

    private static UriMatcher buildUriMatcher() {
        // I know what you're thinking.  Why create a UriMatcher when you can use regular
        // expressions instead?  Because you're not crazy, that's why.

        // All paths added to the UriMatcher have a corresponding code to return when a match is
        // found.  The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher   = new UriMatcher(UriMatcher.NO_MATCH);
        final String     authority = ArtistContract.CONTENT_AUTHORITY;

        // For each type of URI you want to add, create a corresponding code.
        matcher.addURI(authority, ArtistContract.PATH_ARTIST, ARTIST);
        matcher.addURI(authority, ArtistContract.PATH_ARTIST + "/#", ARTIST_BY_ID);
        matcher.addURI(authority, ArtistContract.PATH_FAVORITES, FAVORITES);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        artistDbHelper = new ArtistDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;

        switch (sUriMatcher.match(uri)) {
            case ARTIST_BY_ID:
                long id = ContentUris.parseId(uri);
                String sel = ArtistContract.ArtistEntry.COLUMN_ID + " = ?";
                String[] selArgs = {String.valueOf(id)};
                retCursor = artistDbHelper.getReadableDatabase().query(ArtistContract.ArtistEntry.TABLE_NAME,
                                                                       projection,
                                                                       sel,
                                                                       selArgs,
                                                                       null,
                                                                       null,
                                                                       sortOrder);
                break;
            case ARTIST:
                retCursor = artistDbHelper.getReadableDatabase().query(ArtistContract.ArtistEntry.TABLE_NAME,
                                                                       projection,
                                                                       selection,
                                                                       selectionArgs,
                                                                       null,
                                                                       null,
                                                                       null);
                break;
            case FAVORITES:
                Cursor c = artistDbHelper.getReadableDatabase().query(ArtistContract.FavoriteEntry.TABLE_NAME,
                                                                      projection,
                                                                      selection,
                                                                      selectionArgs,
                                                                      null,
                                                                      null,
                                                                      null
                                                                     );
                String s = ArtistContract.ArtistEntry.COLUMN_ID + " in (";
                int i = 0;
                if (c.moveToFirst()) {
                    // add all id to string, separated with comma
                    do {
                        s += String.valueOf(c.getLong(c.getColumnIndex(ArtistContract.FavoriteEntry.COLUMN_ID)));
                        if (i < c.getCount() - 1) {
                            s += ", ";
                        }
                        i++;
                    } while (c.moveToNext());
                }
                c.close();
                s += ")";
                retCursor = artistDbHelper.getReadableDatabase().query(ArtistContract.ArtistEntry.TABLE_NAME,
                                                                       null,
                                                                       s,
                                                                       null,
                                                                       null,
                                                                       null,
                                                                       null);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case ARTIST_BY_ID:
                return ArtistContract.ArtistEntry.CONTENT_ITEM_TYPE;
            case ARTIST:
                return ArtistContract.ArtistEntry.CONTENT_TYPE;
            case FAVORITES:
                return ArtistContract.FavoriteEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = artistDbHelper.getWritableDatabase();

        Uri       returnUri;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ARTIST:
                long _id = db.insert(ArtistContract.ArtistEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = ArtistContract.ArtistEntry.buildArtistUri(values.getAsLong(ArtistContract.ArtistEntry.COLUMN_ID));
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            case FAVORITES:
                long _idFavorite = db.insert(ArtistContract.FavoriteEntry.TABLE_NAME, null, values);
                if (_idFavorite > 0) {
                    returnUri = ArtistContract.FavoriteEntry.buildFavoriteUri(values.getAsLong(ArtistContract.FavoriteEntry.COLUMN_ID));
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }


    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        SQLiteDatabase database = artistDbHelper.getWritableDatabase();
        if (sUriMatcher.match(uri) != ARTIST) {
            throw new IllegalArgumentException("Wrong URI: " + uri);
        } else {
            int numInserted = 0;
            database.beginTransaction();
            try {
                for (ContentValues contentValues : values) {
                    long id = database.insertWithOnConflict(ArtistContract.ArtistEntry.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                    if (id > 0) {
                        numInserted++;
                    }
                }
                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }
            getContext().getContentResolver().notifyChange(uri, null);
            return numInserted;
        }
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db    = artistDbHelper.getWritableDatabase();
        final int            match = sUriMatcher.match(uri);
        int                  numDeleted;
        switch (match) {
            case ARTIST:
                numDeleted = db.delete(ArtistContract.ArtistEntry.TABLE_NAME, null, null);
                break;
            case FAVORITES:
                numDeleted = db.delete(ArtistContract.FavoriteEntry.TABLE_NAME, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        return numDeleted;

    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}

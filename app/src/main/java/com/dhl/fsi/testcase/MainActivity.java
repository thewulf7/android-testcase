package com.dhl.fsi.testcase;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dhl.fsi.testcase.fragments.ArtistsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            ArtistsFragment artistsFragment = new ArtistsFragment();
            getFragmentManager().beginTransaction().add(R.id.container, artistsFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}

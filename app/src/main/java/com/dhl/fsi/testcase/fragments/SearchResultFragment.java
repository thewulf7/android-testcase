package com.dhl.fsi.testcase.fragments;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.utils.Utils;
import com.lapism.searchview.SearchAdapter;
import com.lapism.searchview.SearchItem;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int    ARTIST_LOADER = 4;
    private static final String QUERY         = "query_artist";

    private CoordinatorLayout coordinatorLayout;
    // displayed when cursor data is empty
    private RelativeLayout    nothingRelativeLayout;

    private String query;


    public SearchResultFragment() {
        // Required empty public constructor
    }

    public static SearchResultFragment newInstance(String query) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle               args     = new Bundle();
        args.putString(QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void showSearchView() {
        super.showSearchView();

        EditText queryTextView = (EditText) searchView.findViewById(com.lapism.searchview.R.id.searchEditText_input);
        queryTextView.setText(query);
        queryTextView.setSelection(0, query.length());
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            query = getArguments().getString(QUERY);
        }
    }

    // set up SearchView
    @Override
    void initSearchView(View view) {
        super.initSearchView(view);

        List<SearchItem> mResultsList = new ArrayList<>();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO: prevent restarting loader with empty string
                query = newText;
                // refresh data according to query
                getLoaderManager().restartLoader(ARTIST_LOADER, null, SearchResultFragment.this);

                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String _query) {
                searchView.close(false);
                historyDatabase.addItem(new SearchItem(_query));
                query = _query;
                getLoaderManager().restartLoader(ARTIST_LOADER, null, SearchResultFragment.this);
                return false;
            }
        });

        // set up SearchAdapter
        SearchAdapter mSearchAdapter = new SearchAdapter(getActivity(), mResultsList);
        mSearchAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                searchView.close(false);
                TextView     textView = (TextView) view.findViewById(R.id.textView_item_text);
                CharSequence text     = textView.getText();
                historyDatabase.addItem(new SearchItem(text));
                query = text.toString();
                // refresh data according to query
                getLoaderManager().restartLoader(ARTIST_LOADER, null, SearchResultFragment.this);
            }
        });
        searchView.setAdapter(mSearchAdapter);
    }

    @Override
    void initToolbar(View view, int resId) {
        super.initToolbar(view, resId);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        // set up toolbar
        initToolbar(view, R.string.artists);

        // set up SearchView
        initSearchView(view);

        initRecyclerView(view);

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);
        nothingRelativeLayout = (RelativeLayout) view.findViewById(R.id.nothing_relative_layout);

        // animate showing fragment
        Utils.animate(view, Utils.Gravity.CENTER_HORIZONTAL, Utils.Gravity.CENTER_VERTICAL, 1000);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(ARTIST_LOADER, null, this);
        if (!Utils.checkInternetConnection(getActivity())) {
            Snackbar.make(coordinatorLayout, getResources().getString(R.string.no_network_connection),
                          Snackbar.LENGTH_LONG).show();
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getActivity(),
                                ArtistContract.ArtistEntry.CONTENT_URI,
                                null,
                                ArtistContract.ArtistEntry.COLUMN_NAME + " LIKE '%" + query + "%'",
                                null,
                                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // update RecyclerView adapter
        adapter.swapCursor(data);
        // update toolbar
        toolbar.setTitle("\"" + query + "\"");
        toolbar.setSubtitle(getResources().getString(R.string.search_results) + data.getCount());

        // TODO: show layout if cursor is empty|not empty
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}

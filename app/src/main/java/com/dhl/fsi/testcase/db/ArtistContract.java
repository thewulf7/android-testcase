package com.dhl.fsi.testcase.db;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.dhl.fsi.testcase.model.Artist;
import com.dhl.fsi.testcase.model.Cover;

import java.util.Arrays;

/**
 * @author Evgenii Utkin
 */
public class ArtistContract {
    public static final  String CONTENT_AUTHORITY = "com.dhl.fsi.testcase";
    public static final  String PATH_ARTIST       = "artist";
    public static final  String PATH_FAVORITES    = "favorites";
    private static final Uri    BASE_CONTENT_URI  = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static ContentValues convertArtistToContentValues(Artist artist) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_ID, artist.getId());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_NAME, artist.getName());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_GENRES, artist.getGenresString());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_TRACKS, artist.getTracks());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_ALBUMS, artist.getAlbums());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_LINK, artist.getLink());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_DESCRIPTION, artist.getDescription());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_COVER_SMALL, artist.getCover().getSmall());
        contentValues.put(ArtistContract.ArtistEntry.COLUMN_COVER_BIG, artist.getCover().getBig());
        return contentValues;
    }

    /**
     * Convert cursor row to artist object
     *
     * @param cursor must be not null and in necessary position
     * @return Artist
     */
    public static Artist convertCursorToArtist(Cursor cursor) {

        return new Artist(cursor.getLong(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_ID)),
                          cursor.getString(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_NAME)),
                          Arrays.asList(cursor.getString(cursor.getColumnIndex(ArtistEntry.COLUMN_GENRES)).split(",")),
                          cursor.getInt(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_TRACKS)),
                          cursor.getInt(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_ALBUMS)),
                          cursor.getString(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_LINK)),
                          cursor.getString(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_DESCRIPTION)),
                          new Cover(cursor.getString(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_COVER_SMALL)),
                                    cursor.getString(cursor.getColumnIndex(ArtistContract.ArtistEntry.COLUMN_COVER_BIG))));

    }

    public static final class ArtistEntry implements BaseColumns {
        public static final String COLUMN_ALBUMS      = "albums";
        public static final String COLUMN_COVER_BIG   = "cover_big";
        public static final String COLUMN_COVER_SMALL = "cover_small";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_GENRES      = "genres";
        public static final String COLUMN_ID          = "artist_id";
        public static final String COLUMN_LINK        = "link";
        public static final String COLUMN_NAME        = "name";
        public static final String COLUMN_TRACKS      = "tracks";
        public static final String CONTENT_ITEM_TYPE  = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ARTIST;
        public static final String CONTENT_TYPE       = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ARTIST;
        public static final Uri    CONTENT_URI        = BASE_CONTENT_URI.buildUpon().appendPath(PATH_ARTIST).build();
        public static final String TABLE_NAME         = "artists";

        /*
        * @return Uri with id
         */
        public static Uri buildArtistUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

    public static final class FavoriteEntry implements BaseColumns {
        public static final String COLUMN_ID    = "artist_id";
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAVORITES;
        public static final Uri    CONTENT_URI  = BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAVORITES).build();
        public static final String TABLE_NAME   = "favorites";

        public static Uri buildFavoriteUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}

package com.dhl.fsi.testcase.fragments;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.dhl.fsi.testcase.R;
import com.dhl.fsi.testcase.db.ArtistContract;
import com.dhl.fsi.testcase.utils.Utils;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int FAVORITES_LOADER = 3;
    // Layout show when data is empty
    private RelativeLayout nothingRelativeLayout;


    public FavoritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        // set up toolbar
        initToolbar(view, R.string.favorite);

        // set up recyclerView
        initRecyclerView(view);

        nothingRelativeLayout = (RelativeLayout) view.findViewById(R.id.nothing_relative_layout);

        // animate showing fragment
        Utils.animate(view, Utils.Gravity.RIGHT, Utils.Gravity.TOP, 500);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(FAVORITES_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    @Override
    protected void initToolbar(View view, int resId) {
        super.initToolbar(view, resId);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        // load all favorites
        return new CursorLoader(getActivity(),
                                ArtistContract.FavoriteEntry.CONTENT_URI,
                                null,
                                null,
                                null,
                                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
        // show layout when data is empty
        if (data.getCount() > 0) {
            nothingRelativeLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            nothingRelativeLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        toolbar.setSubtitle(getResources().getString(R.string.favorites_count) + data.getCount());

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

}

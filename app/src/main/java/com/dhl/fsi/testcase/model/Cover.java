package com.dhl.fsi.testcase.model;

import com.google.gson.annotations.SerializedName;

public class Cover {

    @SerializedName("big")
    private String big;
    @SerializedName("small")
    private String small;

    public Cover(String small, String big) {
        this.small = small;
        this.big = big;
    }

    public String getSmall() {
        return small;
    }

    public String getBig() {
        return big;
    }
}
